package encryptutil

import (
	"encoding/base64"
	"github.com/forgoer/openssl"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRsaEncode(t *testing.T) {
	src := "123456"
	// 公钥使用pks8
	pubKey := "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDb6bxpsivyFxIC8suHnbMIPLYX\nN6amWYY2rarcxnkcyvaECJ2PDLiouo0uc0IFH/Dez8HmN0aL9rZn2za09lnSky9o\nUyJKZlwY7vDmiYeVyuNHvcYLZWT5+0OLbQ22PoEiGrhfCMxzqYdb0FkfDfWU2M/k\nxCAwhn2Hdu04VJz71QIDAQAB\n-----END PUBLIC KEY-----"
	encrypt, err := openssl.RSAEncrypt([]byte(src), []byte(pubKey))
	assert.NoError(t, err)
	toString := base64.StdEncoding.EncodeToString(encrypt)
	t.Log(toString)
}

func TestRsaDecode(t *testing.T) {
	src := "vjzxPjmnsRkSxJfPpGX9ud3CIr7eA0otUnQhmLfDTxst2JCoAa4gBrHRn3G58WeQGFhTxlL5F6xhlQPu6j9RAYz1qUpPDCCpVe8zx2WDPAOkx/2cDB3t5MMP2M1UTXVBTmwz07I7dI6RFuqeIqT9s2D7I/vUYMp1tHypYA41P5M="
	privateKey := "-----BEGIN RSA PRIVATE KEY-----\nMIICXgIBAAKBgQDb6bxpsivyFxIC8suHnbMIPLYXN6amWYY2rarcxnkcyvaECJ2P\nDLiouo0uc0IFH/Dez8HmN0aL9rZn2za09lnSky9oUyJKZlwY7vDmiYeVyuNHvcYL\nZWT5+0OLbQ22PoEiGrhfCMxzqYdb0FkfDfWU2M/kxCAwhn2Hdu04VJz71QIDAQAB\nAoGBAND8woJLwTmStRo6NDOQKVi1oXJU/7lssIB78DlZIDW9qCH3sgwE0eP/TTYM\ncHxAS37jP2iRtShD8Dqod8fnqZkSL6glUUj2Aui120VwiR2xwJDnsYlpRdKSNRoV\nIgH5Dyc0fKoCo0Yq/pZcABL55ITsUXLyXJW4NZeUA04docSNAkEA7VaV7R52Xfxt\n8tUy4kAbwHaXGUFM2NU3pKMLuVVQoQ/qPDpBvm/yBh2ApCX7qqgh+ZsfXDMwY6n3\nwbLU4yVdnwJBAO00ZWtjvDH86qfpKcRDc6IVxMt91qMGIZOIOch6OxOAL+7zg4G5\nI6UHpy6Orl1n+2ycVfL1PdaQmHXpczHOSgsCQGQlsyHZRs0l5Scge1YpAwzVfbC0\ncz7TyaT4/8t2io1L7+T2GCPJjPCzpkKdnHJIe/2dTUBUgUiswdTEJzyp2bUCQQCK\nUpil1AYlvE/2VKB3g8IFjd4xsBMfA+9GghT4FFco2wKYvEY+uoDPtrPGEYwaig1y\n24O/Z0WFPtK5R8ZWD+7bAkEA6r4S4sdispPMjKVwZhi55t+nH1gRNb/f+HTdQFVW\nh0IznBGyVeoDDrsZuGhIOsxpFpeLek5NJVIcqqhUuZFKog==\n-----END RSA PRIVATE KEY-----"
	base64Decode, _ := base64.StdEncoding.DecodeString(src)
	decrypt, err := openssl.RSADecrypt(base64Decode, []byte(privateKey))
	assert.NoError(t, err)
	t.Logf("decrypt：%s", decrypt)
}
