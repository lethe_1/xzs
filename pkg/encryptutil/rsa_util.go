package encryptutil

import (
	"encoding/base64"
	"github.com/forgoer/openssl"
	"xzs/config"
)

// RsaEncode 加密 公钥使用pkcs8填充
func RsaEncode(src string) (string, error) {
	encrypt, err := openssl.RSAEncrypt([]byte(src), []byte(config.GlobalConf.System.PwdKey.PublicKey))
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(encrypt), nil
}

// RsaDecode 解密 私钥使用pkcs1填充，这里要注意对方提供的是pkcs1还是pkcs8，如果是pkcs8需要转为pkcs1
// 在线转换网站http://www.metools.info/code/c87.html，也可以在命令行使用openssl命令行转换，特别要注意的就是格式
func RsaDecode(src string) (string, error) {
	base64Decode, err := base64.StdEncoding.DecodeString(src)
	if err != nil {
		return "", err
	}
	decrypt, err := openssl.RSADecrypt(base64Decode, []byte(config.GlobalConf.System.PwdKey.PrivateKey))
	if err != nil {
		return "", err
	}
	return string(decrypt), nil
}
